﻿#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#define BUFFER_SIZE 512
#define OK_STATE 5

int main() {
    char* stringBuffer = (char*)calloc(BUFFER_SIZE, sizeof(char));
    int currentState = 1;
    bool terminatedState = false;
    char currentChar;
    uint32_t i = 0;

    printf("The expression - a((ab)|(bc))*c\n");
    printf("Let's check if your string matches the expression or not.\n");
    printf("Enter string for recognition (maximum string length is 512 ASCII characters):\n");
    fgets(stringBuffer, BUFFER_SIZE, stdin);

    for (i = 0; i < BUFFER_SIZE; i++) {
        if (stringBuffer[i] == 0 || stringBuffer[i] == '\n') {
            break;
        }

        currentChar = stringBuffer[i];
        switch (currentState) {
        case 1:
            switch (currentChar) {
            case 'a':
                currentState = 2;
                break;
            default:
                fprintf(stderr, "Unexpected symbol: %c!\n", currentChar);
                currentState = -1;
                break;
            }
            break;

        case 2:
            switch (currentChar) {
            case 'a':
                currentState = 3;
                break;
            case 'b':
                currentState = 4;
                break;
            case 'c':
                currentState = 5;
                break;
            default:
                fprintf(stderr, "Unexpected symbol: %c!\n", currentChar);
                currentState = -1;
                break;
            }
            break;

        case 3:
            switch (currentChar) {
            case 'b':
                currentState = 2;
                break;
            default:
                fprintf(stderr, "Unexpected symbol: %c!\n", currentChar);
                currentState = -1;
                break;
            }
            break;

        case 4:
            switch (currentChar) {
            case 'c':
                currentState = 2;
                break;
            default:
                fprintf(stderr, "Unexpected symbol: %c!\n", currentChar);
                currentState = -1;
                break;
            }
            break;

        case 5:
            terminatedState = true;
            break;

        default:
            fprintf(stderr, "Unexpected state: %d!\n", currentState);
            currentState = -1;
            break;
        }

        // -1 states for unexpected symbol or state
        if (currentState == -1 || terminatedState) {
            break;
        }
    }

    if (terminatedState && i < BUFFER_SIZE && stringBuffer[i + 1] == '\n' || stringBuffer[i + 1] == 0) {
        printf("Your string matches the expression! ^_^\n");
    }
    else {
        printf("Your string doesn't match the expression :c\n");
    }

    free(stringBuffer);

    return 0;
}
